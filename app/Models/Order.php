<?php

namespace App\Models;

use App\Models\User;

use App\Models\Salon;
use App\Models\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'salon_id', 'total',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }

    public function salon(): HasOne
    {
        return $this->hasOne(Salon::class);
    }

    public function service(): HasOne
    {
        return $this->hasOne(Service::class);
    }
}
