<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use App\Models\Salonr;
use App\Models\Service;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Order::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => (['integer', 'required']),
            'salon_id' => (['integer', 'required']),
            'total' => ['integer', 'required'],
            'service' => ['required']
        ]);
        $order = Order::create([
            'user_id' => $request->user_id,
            'salon_id' => $request->salon_id,
            'total' => $request->total,
        ]);
        return $order->service()->attach($request->service);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $users = User::all();
        $service = Service::all();
        return $order->load('users');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $request->validate([
            'user_id' => ['required'],
            'salon_id' =>  ['required'],
            'total' => ['required'],
        ]);
        $order->update([
            'user_id' => $request->user_id,
            'salon_id' => $request->salon_id,
            'total' => $request->total,
        ]);
        return $order->service()->sync($request->service);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return 'order was deleted';
    }
}
