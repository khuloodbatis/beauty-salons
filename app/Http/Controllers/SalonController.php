<?php

namespace App\Http\Controllers;

use App\Models\salon;
use Illuminate\Http\Request;

class SalonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Salon::all();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $salon = Salon::create([
            'name' => $request->name,
            'address' => $request->address,
            'mobile' => $request->mobile,
        ]);
        return $salon;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\salon  $salon
     * @return \Illuminate\Http\Response
     */
    public function show(salon $salon)
    {
        return $salon->load('services');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\salon  $salon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, salon $salon)
    {
        $request->validate([
            'name' => ['alpha'],
            'address' =>  ['required'],
            'mobile' => ['required'],
        ]);
        $salon->update([
            'name' => $request->name,
            'address' => $request->address,
            'mobile' => $request->mobile,
        ]);
        return $salon;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\salon  $salon
     * @return \Illuminate\Http\Response
     */
    public function destroy(salon $salon)
    {
        $salon->delete();
    }
}
