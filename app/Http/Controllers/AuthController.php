<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{


    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'alpha'],
            'email' => ['required', 'unique:users,email'],
            'password' => ['required', 'min:6'],
            'age' => ['required'],
            'mobile' => ['required', 'min:8'],
            'gender' => ['required'],
            'address' => ['required'],
            'city' => ['required',],
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'gender' => $request->gender,
            'mobile' => $request->mobile,
            'age' => $request->age,
            'address' => $request->address,
            'city' => $request->city,

        ]);

        return $user->createToken('key')->plainTextToken;
    }

    public function login(Request $request)
    {

        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required', 'min:6'],
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return auth()->user()->createToken('key')->plainTextToken;
        }

        return 'The email or password was invalid';
    }
}
